(* HANGMAN *)
Random.self_init ();;

let is_in_alpabet x =
	if (String.uppercase_ascii (Char.escaped x)) =
		(String.lowercase_ascii (Char.escaped x)) then
		false else true; in

let hangman = [|
"  +---+
  |   |
      |
      |
      |
      |
=========" ; "  +---+
  |   |
  O   |
      |
      |
      |
=========" ; "  +---+
  |   |
  O   |
  |   |
      |
      |
=========" ; "  +---+
  |   |
  O   |
 /|   |
      |
      |
=========" ; "  +---+
  |   |
  O   |
 /|\\  |
      |
      |
=========" ; "  +---+
  |   |
  O   |
 /|\\  |
 /    |
      |
=========" ; "  +---+
  |   |
  O   |
 /|\\  |
 / \\  |
      |
========="|] in

print_endline hangman.(6);

print_endline "HANGMAN FOR CAMELS!";

let winPhrase = (
	let noun1 = (
		begin match (Random.int 2) with
		| 0 -> "sling"
		| 1 -> "bully"
		| 2 -> "greed"
		| _ -> "error!"
		end
	) in

	begin match (Random.int 5) with
	| 0 -> ("the " ^ noun1 ^ " is made of smaller soups.")
	| 1 -> ("tl; dr: " ^ noun1 ^ " fell. i hath risen.")
	| 2 -> ("if only i had a working " ^ noun1 ^ ".")
	| 3 -> ("how was your day? mine had a " ^ noun1 ^ ".")
	| 4 -> ("i need a medical " ^ noun1 ^ ".")
	| 5 -> (noun1 ^ " is like crippling arthropods? spiders.")
	| _ -> ("error!")
	end
) in

let winPhraseLength = String.length winPhrase in
let gameBoardinit = Buffer.create 32 in
	
for i = 0 to (String.length winPhrase) - 1 do 
	if is_in_alpabet winPhrase.[i] then
		Buffer.add_char gameBoardinit '_'
	else
		Buffer.add_char gameBoardinit winPhrase.[i]
done;
let gameBoard = ref (Buffer.contents gameBoardinit) in

print_endline "Difficulty select < (e)asy ; (m)edium ; (h)ard ; (b)alls >";
let g = String.lowercase_ascii (input_line stdin) in

let score = ref begin match g with
| "e" -> (6);
| "m" -> (5);
| "h" -> (4);
| "b" -> (3);
| _ -> (Random.int 3) + 3;
end in

let gameContinue = ref true in
while gameContinue = ref true do  (*GAME LOOP*)
	if winPhrase = !gameBoard then (
		print_endline ("winPhrase: " ^ winPhrase ^ "\nYou Win! Score: " ^ (string_of_int !score));
		gameContinue := false;
	) else (
		print_endline hangman.(6 - !score);
		print_endline ("Take a guess! " ^ (string_of_int !score) ^ " misses left!\n" ^ !gameBoard);
		let isInWinPhrase = ref false in
		let input = input_line stdin in
		begin match input with
		| "quit" -> gameContinue := false
		| "winit" -> print_endline winPhrase; gameContinue := false
		| "duck" -> print_endline "quack quack\n"
		| "all" -> print_endline "No?\n"
		| _ -> if String.length input = 1 then (
			for i = 0 to winPhraseLength - 1 do
				if input.[0] = winPhrase.[i] then (
					gameBoard :=
						(String.sub !gameBoard 0 (i)) ^
						input ^
						(String.sub !gameBoard (i+1) (winPhraseLength-(i+1)));
					isInWinPhrase := true;
				);
			done;
			if isInWinPhrase = ref false then
				score := !score - 1;
			if score = ref 0 then (
				gameContinue := false;
				print_endline "You lost!";
			);
		);
		end;
	);
done;
