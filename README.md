# OCaml Hangman
***

## Name
Hangman in OCaml

## Description
It is a simple game of Hangman, but made with OCaml.

## Visuals
--TODO--

## Usage
Compile and run the main.ml file to try it.

## License
MIT License

## Project status
This project is still ongoing.